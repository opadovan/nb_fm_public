# README #

This repository contains all code to generate figures for the Neuroblastoma Foundation Medicine manuscript.

### Repository Setup ###

* Clone repository to home directory
* Create subdirectories `finalfigures/` and `finalfigures/supplement/`

### For more information, contact: ###

* padovanmeo@email.chop.edu
* ramanp@email.chop.edu